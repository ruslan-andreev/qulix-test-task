const selectScript = () => {
    let selectHeader = document.querySelector('.select__header')
    let selectBody = document.querySelector('.select__body')
    let slectItem = document.querySelectorAll('.select__item')
    let selectCurrent = document.querySelector('.select__current')
    let arrow = document.querySelector('.select__icon-arrow')
    

    selectHeader.addEventListener('click', selectToggle)

    slectItem.forEach(item => {
        item.addEventListener('click', (event)=>{
            selectChoose(event)
        })
    })

    function selectToggle(){
        selectBody.classList.toggle('select__body--active')
        arrow.classList.toggle('select__icon-arrow--active')
    }

    function selectChoose(event){
        let text = event.target.innerText
        selectCurrent.innerText = text
        selectToggle()
    }
}
selectScript()