const simbolCounter = () =>{
    let inputTitle = document.querySelector('.input-text')
    let counter = document.getElementById('title-length')

    inputTitle.addEventListener('input', (event) =>{
        simbolCount(event.target.value)
    })

    function simbolCount(data){
        let length = data.length
        counter.innerText = length
    }
}
simbolCounter()